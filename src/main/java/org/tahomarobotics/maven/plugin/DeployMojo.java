/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.maven.plugin;

import java.util.ArrayList;
import java.util.List;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.maven.repository.RepositorySystem;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

@Mojo(name = "deploy", requiresDependencyResolution=ResolutionScope.COMPILE_PLUS_RUNTIME)
public class DeployMojo extends AbstractMojo {

	@Component
	private MavenProject project;
	
	@Component
	private MavenSession session;
	
	@Component
	private RepositorySystem repo;
	
	@Parameter(required=true)
	private int team;
	
	@Parameter(defaultValue="Admin", required=true)
	private String username;

	@Parameter(defaultValue="", required=true)
	private String password;
	
	private final List<String> addresses = new ArrayList<>();
	
	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		
		// Default mDNS
		addresses.add("roborio-"+team+"-FRC.local"); 
		// 10.TE.AM.2 (default RIO IP)
		addresses.add("10."+ (int)(team / 100) + "." + (team % 100) + ".2");   
        // USB connected robot
        addresses.add("172.22.11.2"); // USB
		
        JSch jsch = new JSch();
        Session session = null;
		for(String address : addresses) {
			session = connect(jsch, address);
		}
		
		if (session == null) {
			getLog().info("Count not connect to robot");
			return;
		}
		
		
	}
	
	private Session connect(JSch jsch, String host)  {
		try {
			Session session = jsch.getSession(username, host, 22);
			session.setPassword(password);
			session.connect(2);
			return session;
		} catch (JSchException e) {
			getLog().error("Failed to create JSCH session with address: " + host, e);
			return null;
		}
	}
}
